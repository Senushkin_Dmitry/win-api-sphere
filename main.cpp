//
//  main.h
//  MCA
//
//  Created by Dmitry Senushkin on 30.09.15.
//  Copyright (c) 2015 Dmitry Senushkin. All rights reserved.
//

#include "MainWindow.h"
#include "CApplication.h"
#include "MWnd.h"

int APIENTRY WinMain(HINSTANCE hinst, HINSTANCE prev, LPSTR cmd, int showcmd)
{
	//������� ���� ����
	MainWindow *wnd = new MainWindow;
	wnd->createGLWindow(L"TestWindow", 1600, 900, 32, false);
	wnd->DrawGLScene();        // ������ �����

	//��������� ����������
	CApplication *app = new CApplication;
	app->Run();
	return 0;
}