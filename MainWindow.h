//
//  MainWindow.h
//  MCA
//
//  Created by Dmitry Senushkin on 30.09.15.
//  Copyright (c) 2015 Dmitry Senushkin. All rights reserved.
//
#pragma once
#include "MWnd.h"


class MainWindow :
	public MWnd
{
public:
	MainWindow();
	~MainWindow();

	bool SwapBuffer();

	LRESULT OnCreate(LPARAM lparam, WPARAM wparam);
	LRESULT OnDestroy(LPARAM lparam, WPARAM wparam);
	LRESULT Activate(LPARAM lparam, WPARAM wparam);
	LRESULT Syscommand(LPARAM lparam, WPARAM wparam);
	LRESULT Close(LPARAM lparam, WPARAM wparam);
	LRESULT KeyDown(LPARAM lparam, WPARAM wparam);
	LRESULT KeyUp(LPARAM lparam, WPARAM wparam);
	LRESULT Size(LPARAM lparam, WPARAM wparam);

};

