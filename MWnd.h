//
//  MWnd.h
//  MCA
//
//  Created by Dmitry Senushkin on 30.09.15.
//  Copyright (c) 2015 Dmitry Senushkin. All rights reserved.
//
#pragma once
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "glu32.lib")
#pragma comment(lib,"glut32.lib")

#include <Windows.h>
#include <GL/GL.h>
#include <GL/GLU.h>
//#include <GL/glaux.h>
#include "Eigen/Dense"

#include <map>

#include "InterfaceForShell.h"

class MWnd
{
public:
	MWnd();
	~MWnd();

	// ��� ��������� �� �������
	typedef LRESULT(MWnd::*FuncPointer)(LPARAM, WPARAM);

	// ��������� ��������� �� �������-����������
	struct POINTER
	{
		MWnd* wnd;				// ��������� �� �����, �������� ����������� ����������
		FuncPointer func;
	};

	bool create(	HWND parent,				// ������������ ����, ���� 0 - �� ������� ����
					LPCWSTR text,				// ��������� ����
					DWORD exstyle, DWORD style,	// ����� ����
					int x, int y, int w, int h,	// ������� � ���������
					UINT id					// ������������� ����
				);
	bool createGLWindow(LPCWSTR title, int width, int height, int bits, bool fullscreenflag);		// ����������� �������� ���� OpenGL
	bool addMenu();																					// ���������� ���� � ����

	static LRESULT CALLBACK _WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam); // ������� �������, � ������� ��������� ��������� ��� ���������

	template<class T>
	bool AddMessage(UINT message, MWnd* wnd, LRESULT(T::*funcpointer)(LPARAM, WPARAM))
	{
		if (!wnd || !funcpointer) return false;

		POINTER msg;
		msg.wnd = wnd;
		msg.func = reinterpret_cast<FuncPointer>(funcpointer);

		_msgmap.insert(std::pair<UINT, POINTER>(message, msg));

		return true;
	};


	GLvoid ReSizeGLScene(GLsizei width, GLsizei height);        // �������� ������ � ���������������� ���� GL
	bool InitGL(GLvoid);										// ��� ��������� ������� OpenGL ���������� �����
	bool DrawGLScene(GLvoid);									// ����� ����� ����������� ��� ����������
	GLvoid KillGLWindow(GLvoid);									// ���������� ���������� ����


protected:
	HGLRC  hRC = NULL;					// ���������� �������� ����������
	HDC  hDC = NULL;					// ��������� �������� ���������� GDI
	HWND _hwnd;							//����� ������ ����									
	HINSTANCE  hInstance;				// ����� ����� �������� ���������� ����������

	float heading;				// ��������� ������� �������
	float yrot;					// ���� ��������
	float xpos, zpos;			// �������� ������
	float walkbiasangle;
	float walkbias;


	bool  keys[256];						// ������, ������������ ��� �������� � �����������
	bool  active = true;					// ���� ���������� ����, ������������� � true �� ���������
	bool  fullscreen = true;				// ���� ������ ����, ������������� � ������������� �� ���������
	std::map<UINT, POINTER> _msgmap;		//����� ���������

};