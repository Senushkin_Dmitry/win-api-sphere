//
//  CApplication.h
//  MCA
//
//  Created by Dmitry Senushkin on 17.11.15.
//  Copyright (c) 2015 Dmitry Senushkin. All rights reserved.
//
#include "InterfaceForShell.h"


int InterfaceForShell::id[20][3] = { { 1,0,4 },{ 0,4,9 },{ 5, 4, 9 },{ 4, 8, 5 },{ 4, 1, 8 },
									{ 8, 1, 10 },{ 8, 10, 3 },{ 5, 8, 3 },{ 5, 3, 2 },{ 2, 3, 7 },
									{ 7, 3, 10 },{ 7, 10, 6 },{ 7, 6, 11 },{ 11, 6, 0 },{ 0, 6, 1 },
									{ 6, 10, 1 },{ 9, 11, 0 },{ 9, 2, 11 },{ 9, 5, 2 },{ 7, 11, 2 } };
Eigen::Vector3d InterfaceForShell::radiusVectorOfCenter = Eigen::Vector3d(0, 0, 0);
double InterfaceForShell::radius = 1;
double InterfaceForShell::V = InterfaceForShell::radius * cos(3. * atan(1.) / 2.5);
double InterfaceForShell::W = InterfaceForShell::radius * sin(3. * atan(1.) / 2.5);
double InterfaceForShell::vertex[12][3] = {	{ -V,0.,W },{ V,0.,W },{ -V,0.,-W },
											{ V,0.,-W },{ 0.,W,V },{ 0.,W,-V },
											{ 0.,-W,V },{ 0. ,-W,-V },{ W,V, 0. },
											{ -W,V,0. },{ W,-V,0. },{ -W,-V,0. } };

InterfaceForShell::InterfaceForShell()
{
}


InterfaceForShell::~InterfaceForShell()
{
}

void InterfaceForShell::Scale(double *v)
{
	double d = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);

	if (d == 0.)
	{
		//MessageBox(0, L"Zero length vector", L"Error", MB_OK);
	}
	else 
	{
		v[0] /= d;
		v[1] /= d;
		v[2] /= d;
		v[0] *= InterfaceForShell::radius;
		v[1] *= InterfaceForShell::radius;
		v[2] *= InterfaceForShell::radius;
	}
	return;
}

void InterfaceForShell::getNorm(double v1[3], double v2[3], double *out)
{
	out[0] = v1[1] * v2[2] - v1[2] * v2[1];
	out[1] = v1[2] * v2[0] - v1[0] * v2[2];		//�� �������� ���������� ���������
	out[2] = v1[0] * v2[1] - v1[1] * v2[0];
	Scale(out);

}

void InterfaceForShell::Split(double *v1, double *v2, double *v3, long depth, std::vector<Eigen::Vector3d> *shell)
{
	double v12[3], v23[3], v31[3];

	if (depth == 0)

	{

		//====== ��������� ���������� ������������� 

		setTria(v1, v2, v3, shell);

		//====== � ����� �� ���� ����������� ������� 

		return;
	}

	for (int i = 0; i< 3; i++) 
	{
		//====== ����� �� ������ �������, 

		//====== ��� ��� ����� ����������� 

		v12[i] = v1[i] + v2[i];
		v23[i] = v2[i] + v3[i];
		v31[i] = v3[i] + v1[i];

	}

	Scale(v12);
	Scale(v23);
	Scale(v31); //====== � ������ ������ ������������ 
	Split(v1, v12, v31, depth - 1, shell);
	Split(v2, v23, v12, depth - 1, shell);
	Split(v3, v31, v23, depth - 1, shell);
	Split(v12, v23, v31, depth - 1, shell);
	
}

void InterfaceForShell::setTria(double *v1, double *v2, double *v3, std::vector<Eigen::Vector3d> *shell)
{

	//====== ������� � ������� �������� ����� �������� 

	glNormal3dv(v1);

	glVertex3dv(v1);

	glNormal3dv(v2);

	glVertex3dv(v2);

	glNormal3dv(v3);

	glVertex3dv(v3);
	
	int counterVertex1 = 0;
	int counterVertex2 = 0;
	int counterVertex3 = 0;

	Eigen::Vector3d vertex1 = Eigen::Vector3d(v1[0] + InterfaceForShell::radiusVectorOfCenter[0], v1[1] + InterfaceForShell::radiusVectorOfCenter[1], v1[2] + InterfaceForShell::radiusVectorOfCenter[2]);
	Eigen::Vector3d vertex2 = Eigen::Vector3d(v2[0] + InterfaceForShell::radiusVectorOfCenter[0], v2[1] + InterfaceForShell::radiusVectorOfCenter[1], v2[2] + InterfaceForShell::radiusVectorOfCenter[2]);
	Eigen::Vector3d vertex3 = Eigen::Vector3d(v3[0] + InterfaceForShell::radiusVectorOfCenter[0], v3[1] + InterfaceForShell::radiusVectorOfCenter[1], v3[2] + InterfaceForShell::radiusVectorOfCenter[2]);

	if (shell->size() != 0)
	{
		for (auto i = 0; i < shell->size(); i++)
		{
			if (shell->at(i)[0] == vertex1[0] && shell->at(i)[1] == vertex1[1] && shell->at(i)[2] == vertex1[2])
				counterVertex1++;

			if (shell->at(i)[0] == vertex2[0] && shell->at(i)[1] == vertex2[1] && shell->at(i)[2] == vertex2[2])
				counterVertex2++;

			if (shell->at(i)[0] == vertex3[0] && shell->at(i)[1] == vertex3[1] && shell->at(i)[2] == vertex3[2])
				counterVertex3++;
		}
	}

	if (!counterVertex1)
		shell->push_back(vertex1);
	if (!counterVertex2)
		shell->push_back(vertex2);
	if (!counterVertex3)
		shell->push_back(vertex3);
}
void InterfaceForShell::createShell(std::vector<Eigen::Vector3d> *shell)
{
	for (int i = 0; i < 20; i++)

	{
		Split(InterfaceForShell::vertex[InterfaceForShell::id[i][0]], InterfaceForShell::vertex[InterfaceForShell::id[i][1]], InterfaceForShell::vertex[InterfaceForShell::id[i][2]], 3, shell);
	}
}