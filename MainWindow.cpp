//
//  MainWindow.h
//  MCA
//
//  Created by Dmitry Senushkin on 30.09.15.
//  Copyright (c) 2015 Dmitry Senushkin. All rights reserved.
//

#include "MainWindow.h"
#include "MWnd.h"


MainWindow::MainWindow()
{
	AddMessage(WM_CREATE, this, &MainWindow::OnCreate);
	AddMessage(WM_DESTROY, this, &MainWindow::OnDestroy);
	AddMessage(WM_ACTIVATE, this, &MainWindow::Activate);
	AddMessage(WM_KEYDOWN, this, &MainWindow::KeyDown);
	AddMessage(WM_SIZE, this, &MainWindow::Size);
}


MainWindow::~MainWindow()
{
}

LRESULT MainWindow::OnCreate(LPARAM lparam, WPARAM wparam)
{
	//MessageBox(0, L"HelloHabr!", L"", 0);
	return 0;
}

LRESULT MainWindow::OnDestroy(LPARAM lparam, WPARAM wparam)
{
	PostQuitMessage(0);
	return 0;
}
LRESULT MainWindow::Activate(LPARAM lparam, WPARAM wparam)
{
	if (!HIWORD(wparam))          // ��������� ��������� �����������
	{
		active = true;          // ��������� �������
	}
	else
	{
		active = false;          // ��������� ������ �� �������
	}
	return true;
}

LRESULT MainWindow::Syscommand(LPARAM lparam, WPARAM wparam)
{
	switch (wparam)            // ������������� ��������� �����
	{
	case SC_SCREENSAVE:        // �������� �� ���������� �����������?
	case SC_MONITORPOWER:        // �������� �� ������� ������� � ����� ���������� �������?
		return 0;          // ������������� ���
	}
}

LRESULT MainWindow::Close(LPARAM lparam, WPARAM wparam)
{
	PostQuitMessage(0);				// ��������� ��������� � ������
	return 0;						// ��������� �����
}

LRESULT MainWindow::KeyDown(LPARAM lparam, WPARAM wparam)
{
	keys[wparam] = true;			// ���� ���, �� ����������� ���� ������ true

	switch (wparam)
	{
	case VK_UP:         // ���� ������ "�����"
						// �������� ������ - � ������������� ���������
						// ������������� �� ��� X ������� ����������� ������
		xpos -= 6*(float)sin(heading*3.14/180) * 0.05f;
		// ������������� �� ��� Z ������� ����������� ������
		zpos -= 6*(float)cos(heading*3.14/180) * 0.05f;
		if (walkbiasangle >= 359.0f)// walkbiasangle>=359?
		{
			walkbiasangle = 0.0f; // ��������� walkbiasangle  0
		}
		else                        // � ��������� ������
		{
			// ���� walkbiasangle < 359 ��������� ��� �� 10
			walkbiasangle += 10;
		}
		//walkbias = (float)sin(walkbiasangle * 3.14/180) / 20.0f;
		DrawGLScene();      // �������������� �����
		break;

	case VK_DOWN:           // ���� "����"
							// �������� ����� - � ������������� ���������
							// ������������� �� ��� X ������� ����������� ������
		xpos += 6*(float)sin(heading*3.148/180) * 0.05f;
		// ������������� �� ��� Z ������� ����������� ������
		zpos += 6*(float)cos(heading*3.14/180) * 0.05f;
		if (walkbiasangle <= 1.0f)    // walkbiasangle<=1?
		{
			walkbiasangle = 359.0f; // ��������� walkbiasangle 359
		}
		else                          // � ��������� ������
		{
			// ���� walkbiasangle >1 ��������� ��� �� 10
			walkbiasangle -= 10;
		}
		// ��������� ������� ��������
		//walkbias = (float)sin(walkbiasangle * 3.14/180) / 20.0f;
		DrawGLScene();          // �������������� �����
		break;
	case VK_LEFT:
		heading += 1.0f;
		yrot = heading;              // ������� ����� ������
		DrawGLScene();
		break;
	case VK_RIGHT:
		heading -= 1.0f;
		yrot = heading;              // ������� ����� �����
		DrawGLScene();
		break;
	}

	return 0;						// ������������
}

LRESULT MainWindow::KeyUp(LPARAM lparam, WPARAM wparam)
{
	keys[wparam] = false;			//  ���� ���, �� ����������� ���� ������ false
	return 0;						// ������������
}

LRESULT MainWindow::Size(LPARAM lparam, WPARAM wparam)
{
	ReSizeGLScene(LOWORD(lparam), HIWORD(lparam));  // ������� �����=Width, ������� �����=Height
	DrawGLScene();
	return 0;										// ������������
}
bool MainWindow::SwapBuffer()
{
	SwapBuffers(hDC);
	return true;
}