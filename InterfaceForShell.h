//
//  CApplication.h
//  MCA
//
//  Created by Dmitry Senushkin on 17.11.15.
//  Copyright (c) 2015 Dmitry Senushkin. All rights reserved.
//
#pragma once

#include "MWnd.h"
#include <cmath>
#include <vector>
#include "Eigen\Dense"



class InterfaceForShell
{
public:
	static int id[20][3]; // ������ ������� ������ ���������
	static double V, W;		// ��������� ���������
	static double vertex[12][3];
	static double radius;
	static Eigen::Vector3d radiusVectorOfCenter; // center of cell in time of create

	void createShell(std::vector<Eigen::Vector3d> *shell); // �������� �������� ����� ������� �������������
	void Split(double *v1, double *v2, double *v3, long depth, std::vector<Eigen::Vector3d> *shell);
	void setTria(double *v1, double *v2, double *v3, std::vector<Eigen::Vector3d> *shell);
	void Scale(double *v); // ������������ ������� ������� ( ������ ������� )
	void getNorm(double v1[3], double v2[3], double *out);	// ����� ������� (��������� ������ �������)
	InterfaceForShell();
	~InterfaceForShell();
};

