//
//  MWnd.cpp
//  MCA
//
//  Created by Dmitry Senushkin on 30.09.15.
//  Copyright (c) 2015 Dmitry Senushkin. All rights reserved.
//
#include "MWnd.h"
#include <string>
#include <fstream>
#include <iostream>


MWnd::MWnd()
{
}


MWnd::~MWnd()
{
}

bool MWnd::create(HWND parent, LPCWSTR text, DWORD exstyle, DWORD style, int x, int y, int w, int h, UINT id)
{
	// ������������ ����� ����
	WNDCLASSEX wndc;
	wndc.lpszClassName = L"MWnd";
	wndc.cbSize = sizeof(WNDCLASSEX);
	wndc.lpfnWndProc = WNDPROC(_WndProc);//������� ���������
	wndc.cbClsExtra = 0;
	wndc.cbWndExtra = 0;
	wndc.hbrBackground = HBRUSH(COLOR_WINDOW);//���� ���� ����
	hInstance = GetModuleHandle(0);//����� ����������
	wndc.hCursor = LoadCursor(0, IDC_ARROW);//��������� ������������ ������
	wndc.style = CS_HREDRAW | CS_VREDRAW;
	wndc.hIcon = 0;
	wndc.hIconSm = 0;
	wndc.lpszMenuName = 0;
	wndc.hInstance = GetModuleHandle(0);
	RegisterClassEx(&wndc);

	//������� ���� ����
	_hwnd = CreateWindowEx(exstyle, L"MWnd", text,
		style | WS_CLIPCHILDREN,//����� WS_CLIPCHILDREN ����� ��� ����, ����� �������� �������� �� ������ ��� �����������
		x, y, w, h, parent, HMENU(id),
		GetModuleHandle(0),
		this//�������� � ������� ������� ��������� �� ����� ������ ����
		);

	if (!_hwnd) return false;
	return true;
}

bool MWnd::createGLWindow(LPCWSTR title, int width, int height, int bits, bool fullscreenflag)
{
	GLuint    PixelFormat;              // ������ ��������� ����� ������
	WNDCLASS  wndc;					// ��������� ������ ����

	DWORD    dwExStyle;					// ����������� ����� ����
	DWORD    dwStyle;					// ������� ����� ����

	RECT WindowRect;						// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left = (long)0;              // ���������� ����� ������������ � 0
	WindowRect.right = (long)width;         // ���������� ������ ������������ � Width
	WindowRect.top = (long)0;               // ���������� ������� ������������ � 0
	WindowRect.bottom = (long)height;       // ���������� ������ ������������ � Height

	fullscreen = fullscreenflag;            // ������������� �������� ���������� ���������� fullscreen

	wndc.hInstance = GetModuleHandle(0);
	hInstance = GetModuleHandle(NULL);						// ������� ���������� ������ ����������
	wndc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;		// ���������� ��� ����������� � ������ ������� DC
	wndc.lpfnWndProc = WNDPROC(_WndProc);					// ��������� ��������� ���������
	wndc.cbClsExtra = 0;									// ��� �������������� ���������� ��� ����
	wndc.cbWndExtra = 0;									// ��� �������������� ���������� ��� ����
	wndc.hInstance = GetModuleHandle(0);								// ������������� ����������
	wndc.hIcon = LoadIcon(NULL, IDI_WINLOGO);				// ��������� ������ �� ���������
	wndc.hCursor = LoadCursor(NULL, IDC_ARROW);				// ��������� ��������� �����
	wndc.hbrBackground = NULL;								// ��� �� ��������� ��� GL
	wndc.lpszMenuName = NULL;
	//wndc.cbSize = sizeof(WNDCLASSEX);// ���� � ���� �� �����
	wndc.lpszClassName = L"MWndOpenGL";							// ������������� ��� ������

	if (!RegisterClass(&wndc))              // �������� ���������������� ����� ����
	{
		MessageBox(NULL, L"Failed To Register The Window Class.", L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;                // ����� � ����������� �������� �������� false
	}

	if (fullscreen)                // ������������� �����?
	{
		DEVMODE dmScreenSettings;									// ����� ����������
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));		// ������� ��� �������� ���������
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);			// ������ ��������� Devmode
		dmScreenSettings.dmPelsWidth = width;						// ������ ������
		dmScreenSettings.dmPelsHeight = height;						// ������ ������
		dmScreenSettings.dmBitsPerPel = bits;						// ������� �����
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;// ����� �������

		// �������� ���������� ��������� ����� � �������� ���������.  ����������: CDS_FULLSCREEN ������� ������ ����������.
		if (ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)
		{
			// ���� ������������ � ������������� ����� ����������, ����� ���������� ��� ��������: ������� ����� ��� �����.
			if (MessageBox(NULL, L"The Requested Fullscreen Mode Is Not Supported By\nYour Video Card. Use Windowed Mode Instead?",
				L"FullScreenError", MB_YESNO | MB_ICONEXCLAMATION) == IDYES)
			{
				fullscreen = false;          // ����� �������� ������ (fullscreen = false)
			}
			else
			{
				// ������������� ����, ���������� ������������ � �������� ����.
				MessageBox(NULL, L"Program Will Now Close.", L"ERROR", MB_OK | MB_ICONSTOP);
				return false;            // ����� � ����������� �������� false
			}
		}
	}

	if (fullscreen)                  // �� �������� � ������������� ������?
	{
		dwExStyle = WS_EX_APPWINDOW;			// ����������� ����� ����
		dwStyle = WS_POPUP;						// ������� ����� ����
		//ShowCursor(false);					// ������ ��������� �����
	}
	else
	{
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// ����������� ����� ����
		dwStyle = WS_OVERLAPPEDWINDOW;							// ������� ����� ����
	}

	AdjustWindowRectEx(&WindowRect, dwStyle, false, dwExStyle);      // ��������� ���� ���������� �������


	if (!(_hwnd = CreateWindowEx(dwExStyle,         // ����������� ����� ��� ����
		L"MWndOpenGL",								// ��� ������
		title,										// ��������� ����
		WS_CLIPSIBLINGS |							// ��������� ����� ��� ����
		WS_CLIPCHILDREN |							// ��������� ����� ��� ����
		dwStyle,									// ���������� ����� ��� ����
		0, 0,										// ������� ����
		WindowRect.right - WindowRect.left,			// ���������� ���������� ������
		WindowRect.bottom - WindowRect.top,			// ���������� ���������� ������
		NULL,										// ��� �������������
		NULL,										// ��� ����
		hInstance,									// ���������� ����������
		this)))										// �� ������� ������ �� WM_CREATE (???)
	{
		KillGLWindow();                // ������������ �����
		MessageBox(NULL, L"Window Creation Error.", L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;                // ������� false
	}

	static  PIXELFORMATDESCRIPTOR pfd =            // pfd �������� Windows ����� ����� ����� �� ����� ������� �������
	{
		sizeof(PIXELFORMATDESCRIPTOR),            // ������ ����������� ������� ������� ��������
		1,									// ����� ������
		PFD_DRAW_TO_WINDOW |				// ������ ��� ����
		PFD_SUPPORT_OPENGL |				// ������ ��� OpenGL
		PFD_DOUBLEBUFFER,					// ������ ��� �������� ������
		PFD_TYPE_RGBA,						// ��������� RGBA ������
		bits,								// ���������� ��� ������� �����
		0, 0, 0, 0, 0, 0,					// ������������� �������� �����
		0,									// ��� ������ ������������
		0,									// ��������� ��� ������������
		0,									// ��� ������ ����������
		0, 0, 0, 0,							// ���� ���������� ������������
		32,									// 32 ������ Z-����� (����� �������)
		0,									// ��� ������ ���������
		0,									// ��� ��������������� �������
		PFD_MAIN_PLANE,						// ������� ���� ���������
		0,									// ���������������
		0, 0, 0								// ����� ���� ������������
	};

	if (!(hDC = GetDC(_hwnd)))              // ����� �� �� �������� �������� ����������?
	{
		KillGLWindow();                // ������������ �����
		MessageBox(NULL, L"Can't Create A GL Device Context.", L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;                // ������� false
	}

	if (!(PixelFormat = ChoosePixelFormat(hDC, &pfd)))        // ������ �� ���������� ������ �������?
	{
		KillGLWindow();                // ������������ �����
		MessageBox(NULL, L"Can't Find A Suitable PixelFormat.", L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;                // ������� false
	}

	if (!SetPixelFormat(hDC, PixelFormat, &pfd))          // �������� �� ���������� ������ �������?
	{
		KillGLWindow();                // ������������ �����
		MessageBox(NULL, L"Can't Set The PixelFormat.", L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;                // ������� false
	}

	if (!(hRC = wglCreateContext(hDC)))          // �������� �� ���������� �������� ����������?
	{
		KillGLWindow();                // ������������ �����
		MessageBox(NULL, L"Can't Create A GL Rendering Context.", L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;                // ������� false
	}

	if (!wglMakeCurrent(hDC, hRC))            // ����������� ������������ �������� ����������
	{
		KillGLWindow();                // ������������ �����
		MessageBox(NULL, L"Can't Activate The GL Rendering Context.", L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;                // ������� false
	}
	ShowWindow(_hwnd, SW_SHOW);              // �������� ����
	SetForegroundWindow(_hwnd);              // ������ ������� ���������
	SetFocus(_hwnd);                // ���������� ����� ���������� �� ���� ����
	ReSizeGLScene(width, height);              // �������� ����������� ��� ������ OpenGL ������.

	addMenu();

	return true;
}

bool MWnd::addMenu()
{
	HMENU hMainMenu = CreateMenu();
	HMENU hPopMenuFile = CreatePopupMenu();

	AppendMenu(hMainMenu, MF_STRING | MF_POPUP, (UINT)hPopMenuFile, L"����");
	AppendMenu(hMainMenu, MF_STRING, 1000, L"�������");

	AppendMenu(hPopMenuFile, MF_STRING, 1001, L"��������� ������");
	AppendMenu(hPopMenuFile, MF_STRING, 1002, L"������� ������");

	SetMenu(_hwnd, hMainMenu);
	SetMenu(_hwnd, hPopMenuFile);

	return true;
}

LRESULT CALLBACK MWnd::_WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	MWnd *wnd = 0;
	// ��������� WM_NCCREATE �������� �� WM_CREATE
	// �.� ��� �������� ���� �������

	if (message == WM_NCCREATE)
	{
		// �������� ��������� �� ��������� ������ ����, ������� �� �������� � ������� CreateWindowEx
		wnd = (MWnd*)LPCREATESTRUCT(lparam)->lpCreateParams;
		// � ��������� � ���� GWL_USERDATA
		SetWindowLong(hwnd, GWL_USERDATA, LONG(LPCREATESTRUCT(lparam)->lpCreateParams));
		wnd->_hwnd = hwnd;

	}

	// ������ �������� ��������� �� ��� ��������� ����, �� ��� �� ���� GWL_USERDATA
	wnd = (MWnd*)GetWindowLong(hwnd, GWL_USERDATA);

	if (wnd)
	{
		//���� ��������� � �����
		std::map<UINT, POINTER>::iterator it;
		it = wnd->_msgmap.find(message);

		//���� ��������� �� �������, �� ������������ ��� �� ���������
		if (it == wnd->_msgmap.end()) return DefWindowProc(hwnd, message, wparam, lparam);
		else
		{
			POINTER msg = it->second;
			//�������� ������� ����������        
			LRESULT result = (msg.wnd->*msg.func)(lparam, wparam);
			if (result) return result;
		}
	}

	return DefWindowProc(hwnd, message, wparam, lparam);
}

GLvoid MWnd::ReSizeGLScene(GLsizei width, GLsizei height)        // �������� ������ � ���������������� ���� GL
{
	if (height == 0)              // �������������� ������� �� ����
	{
		height = 1;
	}

	glViewport(0, 0, width, height);			// ����� ������� ������� ������
	glMatrixMode(GL_PROJECTION);				// ����� ������� ��������
	glLoadIdentity();							// ����� ������� ��������

												// ���������� ����������� �������������� �������� ��� ����
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	glMatrixMode(GL_MODELVIEW);				// ����� ������� ���� ������
	glLoadIdentity();						// ����� ������� ���� ������
}

bool MWnd::InitGL(GLvoid)                // ��� ��������� ������� OpenGL ���������� �����
{
	glShadeModel(GL_SMOOTH);						// ��������� ������� �������� �����������
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);			// ������� ������ � ������ ����

	glClearDepth(1.0f);					// ��������� ������� ������ �������
	glEnable(GL_DEPTH_TEST);            // ��������� ���� �������
	glDepthFunc(GL_LEQUAL);				// ��� ����� �������

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);      // ��������� � ���������� �����������

	return true;						// ������������� ������ �������
}

bool MWnd::DrawGLScene(GLvoid)                // ����� ����� ����������� ��� ����������
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);			// �������� ����� � ����� �������
	glLoadIdentity();											// �������� ������� �������

	GLfloat xtrans = -xpos;       // �������� ������ �� ��� X
	GLfloat ztrans = -zpos;       // �������� ������ �� ��� Z
								  // ��� �������� ����������� ����� � ����
	GLfloat ytrans = -walkbias - 0.25f;
	// 360 ��������� ���� ��� �������� ������
	GLfloat sceneroty = 360.0f - yrot;
	glTranslatef(0.0f, 3.0f, -20.0f);         // ��������� ����� �� 6 ������� �
											  // � ����� �� 20
									  // ������� � ������������ � ������������ ������� ������
	glRotatef(sceneroty, 0, 1.0f, 0);

	// ������������ ����� ������������ ������
	glTranslatef(xtrans,-4, ztrans);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glColor3f(1.0f, 1.0f, 0.0f);

	

	InterfaceForShell config;
	std::vector<Eigen::Vector3d> * shell = new std::vector<Eigen::Vector3d>;

	//====== �������� ������� ���� ��������� 

	glColor3d(1.0f, 1.0f, 0.0f);
	glBegin(GL_TRIANGLES);
	config.createShell(shell);

	/*for (auto i = 0; i < 4; i++)
	{
		for (auto j = 0; j < 4; j++)
		{
			for (auto k = 0; k < 4; k++)
			{
				glVertex3f(i, j, k);
			}
		}
	}*/

	glEnd();
	
	SwapBuffers(hDC);
	

	return true;												// ���������� ������ �������
}

GLvoid MWnd::KillGLWindow(GLvoid)              // ���������� ���������� ����
{
	if (fullscreen)
	{
		ChangeDisplaySettings(NULL, 0);				// ���� ��, �� ������������� ������� � ������� �����
		ShowCursor(true);							// �������� ������ �����
	}

	if (hRC)										// ���������� �� �������� ����������
	{
		if (!wglMakeCurrent(NULL, NULL))			// �������� �� ���������� RC � DC?
		{
			MessageBox(NULL, L"Release Of DC And RC Failed.", L"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		}
		if (!wglDeleteContext(hRC))					// �������� �� ������� RC?
		{
			MessageBox(NULL, L"Release Rendering Context Failed.", L"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		}
		hRC = NULL;
	}
	if (hDC && !ReleaseDC(_hwnd, hDC))				// �������� �� ���������� DC?
	{
		MessageBox(NULL, L"Release Device Context Failed.", L"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		hDC = NULL;									// ���������� DC � NULL
	}
	if (_hwnd && !DestroyWindow(_hwnd))				// �������� �� ���������� ����?
	{
		MessageBox(NULL, L"Could Not Release hWnd.", L"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		_hwnd = NULL;								// ���������� hWnd � NULL
	}
	if (!UnregisterClass(L"OpenGL", hInstance))     // �������� �� ����������������� �����
	{
		MessageBox(NULL, L"Could Not Unregister Class.", L"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		hInstance = NULL;							// ���������� hInstance � NULL
	}
}