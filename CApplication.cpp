//
//  CApplication.cpp
//  MCA
//
//  Created by Dmitry Senushkin on 30.09.15.
//  Copyright (c) 2015 Dmitry Senushkin. All rights reserved.
//

#include "CApplication.h"


CApplication::CApplication()
{
}


CApplication::~CApplication()
{
}

void CApplication::Run()
{
	MSG msg;
	while (GetMessage(&msg, 0, 0, 0) != 0)
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}
