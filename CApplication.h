//
//  CApplication.h
//  MCA
//
//  Created by Dmitry Senushkin on 30.09.15.
//  Copyright (c) 2015 Dmitry Senushkin. All rights reserved.
//
#pragma once

#include <Windows.h>

class CApplication
{
public:
	CApplication();
	~CApplication();

	void Run();
};

